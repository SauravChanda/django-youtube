### User stories:

* login / register
* Users can upload videos - One video can belong to one or more categories
* Users can watch a video (video detail page)
* Users can list all videos belonging to a single category
* Users can comment on videos

