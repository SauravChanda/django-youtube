### Schema Design

### Tables required

- User (default django user)
- Videos
- Categories
- Comments

### Relations

- one User can have many Videos
- one Video will have one Owner(User)
- one Video can have many Comments
- one Comment belongs to one Video only
- one Video can belong to one or more Categories
- one Category may consist of one or more Videos

### Videos

| id  | name | description | location | Category(fk) | User(fk) |
| --- | ---- | ----------- | -------- | ------------ | -------- |


### Categories

| id  | name |
| --- | ---- |


### Comments

| id  | User(fk) | content | Video(fk) |
| --- | -------- | ------- | --------- |

