from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

# Create your models here.
class Category(models.Model):
    name = models.CharField(max_length=200, unique=True)


class Video(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    location = models.FilePathField(default="videos/default/default.jpeg")
    categories = models.ManyToManyField(Category)
    created_at = models.DateTimeField(auto_now_add=True)


class Comment(models.Model):
    content = models.TextField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    video = models.ForeignKey(Video, on_delete=models.CASCADE)
    timestamp = models.DateTimeField(auto_now_add=True)
